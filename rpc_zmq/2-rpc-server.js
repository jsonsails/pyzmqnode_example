var zmq = require('zmq')
var sock = zmq.socket('rep')
sock.bindSync("tcp://127.0.0.1:5556")

sock.on('message', function(msg){
  var decoded = decode_rpc_req(msg)
  var code = decoded.method
  var params = decoded.params
  var func = eval_func(code)
  var result = func.apply(undefined, params)
  sock.send(encode_rpc_rep(result))
});

function encode_rpc_rep(result, error){
    // note: no error handling!
    var rep = {};
    if(result === undefined){
        rep.error = error;
    }
    if(error === undefined){
        rep.result = result;
    }
    return JSON.stringify(rep);
}

function decode_rpc_req(encoded){
    var dec = JSON.parse(encoded);
    return dec;
}

function eval_func(code_str){
    var f = eval("(" + code_str + ")");
    return f;
}