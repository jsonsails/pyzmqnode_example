The intent of this directory is to chronicle the iterations of developing a
RPC interface which uses ZeroMQ to communicate from a Python process into
NodeJS and executes arbitrary serialized Javascript code.

Add this repo to the python path in order to be able to import the rpc module.

The first step is to produce the RPC interface in pure python.

Recipe for dynamic functions in python:
    >>> exec("def f(): return 1",{},d)
    >>> d.keys()
    dict_keys(['f'])
    >>> a = next(iter(d.values()))
    >>> a()
    1

This is demonstrated in 1-rpc-wrapper.py

2-rpc-client/server demonstrate python passing a serialized js function and
parameters to Node which runs it and passes back the result.