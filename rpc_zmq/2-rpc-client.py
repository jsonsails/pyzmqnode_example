__author__ = 'eujain'
import rpc_zmq.rpc as rpc

def main():
    client = rpc.RpcService("tcp://127.0.0.1:5556")
    resp = client.send(
        # note: whitespace matters here
"""
(function f(h){
    return h + " world";})
""",
        ["hello"]
    )
    print(resp)

if __name__ == "__main__":
    main()
