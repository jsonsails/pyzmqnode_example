import zmq
import json
from multiprocessing import Process


class serverProcess(Process):
    def __init__(self):
        super().__init__()
        self.methods = {
            "foo": foo,
            "add": add
        }

    def run(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("tcp://127.0.0.1:5556")
        while True:
            msg = self.socket.recv_string()
            method, params = decode_rpc_req(msg)
            if method not in self.methods:
                self.socket.send_string(
                    encode_rpc_rep(None, "method not found"))
            else:
                result = self.methods[method](*params)
                self.socket.send_string(encode_rpc_rep(result))

def foo(*args):
    return " ".join(args) + " world"

def add(*args):
    return sum(args)


def rpc(sock, func, params=list()):
    # encapsulate a remote function using ZMQ
    sock.send_string(
        encode_rpc_req(func, params)
    )
    # this line blocks
    return sock.recv_string()

def encode_rpc_req(func, params=list()):
    # return string in jsonrpc format http://json-rpc.org/wiki/specification
    req = {
        "method": func,
        "params": params,
        #"id": 1
    }
    return json.dumps(req)

def decode_rpc_req(encoded):
    dec = json.loads(encoded)
    return (dec['method'], dec['params'])

def encode_rpc_rep(result, error=None):
    rep = {}
    if result is not None:
        rep["result"] = result
    if error is not None:
        rep["error"] = error
    return json.dumps(rep)


def main():

    server = serverProcess()
    server.start()

    context = zmq.Context()

    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:5556")

    msg = rpc(socket, "foo", ["hello"])
    print(msg)

    msg = rpc(socket, "add", [1,2,3,4,5])
    print(msg)
    server.terminate()

if __name__ == "__main__":
    main()
