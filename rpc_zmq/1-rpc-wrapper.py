import rpc_zmq.rpc as rpc
import multiprocessing

def server_proc():
    server = rpc.RpcServer("tcp://127.0.0.1:5556")
    server.run()


def main():
    server = multiprocessing.Process(target=server_proc)
    server.start()

    client = rpc.RpcService("tcp://127.0.0.1:5556")
    resp = client.send(
        # note: whitespace matters here
"""
def f(h):
    return h + " world"
""",
        ["hello"]
    )
    print(resp)

    server.terminate()

if __name__ == "__main__":
    main()
