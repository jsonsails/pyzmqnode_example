__author__ = 'eujain'
import zmq
import json

class RpcService:
    """ client side
    """
    def __init__(self, target, context=None):
        self.context = context or zmq.Context.instance()
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect(target)

    def send(self, func, params):
        """func is a string containing python code to be eval'ed
        params is list of parameters
        """
        msg = encode_rpc_req(func, params)
        self.socket.send_string(msg)
        resp = self.socket.recv_string()
        return resp

class RpcServer:
    """Server side
    """
    def __init__(self, target, context=None):
        self.context = context or zmq.Context.instance()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind(target)

    def run(self):
        while True:
            msg = self.socket.recv_string()
            try:
                func_code, params = decode_rpc_req(msg)
                func = eval_func(func_code)
                result = func(*params)
                self.socket.send_string(encode_rpc_rep(result))
            except Exception as e:
                self.socket.send_string(encode_rpc_rep(None, str(e)))


def eval_func(code_str):
    # take a string which is a valid function and evaluates it. returns function
    d = {}
    exec(code_str, {}, d)
    f = next(iter(d.values()))
    return f

def encode_rpc_req(func, params=list()):
    # return string in jsonrpc format http://json-rpc.org/wiki/specification
    req = {
        "method": func,
        "params": params,
        #"id": 1
    }
    return json.dumps(req)

def decode_rpc_req(encoded):
    dec = json.loads(encoded)
    return (dec['method'], dec['params'])

def encode_rpc_rep(result, error=None):
    rep = {}
    if result is not None:
        rep["result"] = result
    if error is not None:
        rep["error"] = error
    return json.dumps(rep)