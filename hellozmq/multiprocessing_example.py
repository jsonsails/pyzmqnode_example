import zmq
from multiprocessing import Process
import time
""" This is an example of Python to Python process zeroMQ ipc """

class serverProcess(Process):
    def __init__(self):
        super().__init__()

    def run(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("tcp://127.0.0.1:5556")
        while True:
            msg = self.socket.recv_string()
            self.socket.send_string(msg + " world")

def main():

    server = serverProcess()
    server.start()

    context = zmq.Context()

    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:5556")

    socket.send_string("hello")
    msg = socket.recv_string()
    print(msg)
    server.terminate()

if __name__ == "__main__":
    main()
