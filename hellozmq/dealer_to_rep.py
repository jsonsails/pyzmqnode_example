__author__ = 'eujain'
""" Dealer Wrapper: a class that defines an async interface between
independent worker processes and a client thread pool.

Note: dealer and router sockets sending to a REP socket must include a null
"" message segment between identity(routing info) and message.
 """

from multiprocessing import Process
import threading
import zmq
import os
import sys

#
#   Request-reply client in Python
#   Connects REQ socket to tcp://localhost:5559
#   Sends "Hello" to server, expects "World" back
#



def tprint(msg):
    """like print, but won't get newlines confused with multiple threads"""
    sys.stdout.write(msg + '\n')
    sys.stdout.flush()

class ClientTask(threading.Thread):
    """ClientTask"""

    def __init__(self, id):
        self.id = id
        threading.Thread.__init__(self)

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.DEALER)
        identity = 'client-{}'.format(self.id)
        socket.set_string(zmq.IDENTITY, identity)
        socket.connect('tcp://localhost:5570')
        tprint('Client %s started' % (identity))
        poll = zmq.Poller()
        poll.register(socket, zmq.POLLIN)

        for reqs in range(1, 10):
            tprint('Req #%d sent..' % (reqs))
            socket.send_string("", zmq.SNDMORE)
            socket.send_string('Hello #%d' % (reqs))
            for i in range(5):
                sockets = dict(poll.poll(1000))
                if socket in sockets:
                    msg = socket.recv()
                    tprint('Client %s received: %s' % (identity,
                                                       msg.decode()))
                    break

        socket.close()
        context.term()

def worker_main():
    #
    #   Request-reply service in Python
    #   Connects REP socket to tcp://localhost:5560
    #   Expects "Hello" from client, replies with "World"
    #
    from time import sleep
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.connect("tcp://localhost:5561")
    print("worker connected.")

    while True:
        message = socket.recv()
        # fake processing
        sleep(0.3)
        pid = os.getpid()
        print("%s Received request: %s" % (pid, message))
        socket.send_string(message.decode() + " World")


class Broker_Process(Process):
    """ServerTask"""

    def __init__(self):
        Process.__init__(self)

    def run(self):
        context = zmq.Context()
        frontend = context.socket(zmq.ROUTER)
        frontend.bind('tcp://*:5570')

        backend = context.socket(zmq.DEALER)
        backend.bind('tcp://*:5561')

        poll = zmq.Poller()
        poll.register(frontend, zmq.POLLIN)
        poll.register(backend, zmq.POLLIN)

        print("broker started")

        while True:
            sockets = dict(poll.poll())
            if frontend in sockets:
                ident, null, msg = frontend.recv_multipart()
                #tprint('Server received %s id %s' % (msg, ident))
                backend.send_multipart([ident, null, msg])
            if backend in sockets:
                ident, null, msg = backend.recv_multipart()
                #tprint('Sending to frontend %s id %s' % (msg, ident))
                frontend.send_multipart([ident, msg])

        frontend.close()
        backend.close()
        context.term()


if __name__ == '__main__':
    from time import sleep
    from threading import Thread
    # use multiple client threads to simulate Django threads
    broker = Broker_Process()
    broker.start()

    client_threads = [ClientTask(x) for x in range(3)]

    worker_pool = [Process(target=worker_main) for x in range(3)]


    for worker in worker_pool:
        worker.start()


    for client in client_threads:
        client.start()
    broker.join()