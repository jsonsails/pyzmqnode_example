"""
This is the client - run nodejs helloworld.js first.
"""

import zmq

print(zmq.zmq_version())

context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://127.0.0.1:5556")

socket.send_string("hello")
msg = socket.recv_string()
print(msg)