__author__ = 'eujain'
""" Show example of multiple workers and clients behaving asynchronously.
 Use case is having several Node workers going at once.

 Dealer and Router pattern is supposed to allow asynch behavior but this
 client still waits for a response before sending another request.
 """

from multiprocessing import Process
import zmq
import os

#
#   Request-reply client in Python
#   Connects REQ socket to tcp://localhost:5559
#   Sends "Hello" to server, expects "World" back
#

def client_main():
    #  Prepare our context and sockets
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5559")

    #  Do 10 requests, waiting each time for a response
    for request in range(1,31):
        socket.send(b"Hello")
        message = socket.recv()
        print("Received reply %s [%s]" % (request, message))

def worker_main():
    #
    #   Request-reply service in Python
    #   Connects REP socket to tcp://localhost:5560
    #   Expects "Hello" from client, replies with "World"
    #
    from random import random
    from time import sleep
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.connect("tcp://localhost:5560")

    while True:
        message = socket.recv()
        # fake processing
        sleep(random()/10)
        pid = os.getpid()
        print("%s Received request: %s" % (pid, message))
        socket.send(b"World")

def glue_main():
    # Simple request-reply broker
    #
    # Author: Lev Givon <lev(at)columbia(dot)edu>

    # Prepare our context and sockets
    context = zmq.Context()
    frontend = context.socket(zmq.ROUTER)
    backend = context.socket(zmq.DEALER)
    frontend.bind("tcp://*:5559")
    backend.bind("tcp://*:5560")

    # Initialize poll set
    poller = zmq.Poller()
    poller.register(frontend, zmq.POLLIN)
    poller.register(backend, zmq.POLLIN)

    # Switch messages between sockets
    while True:
        socks = dict(poller.poll())

        if socks.get(frontend) == zmq.POLLIN:
            message = frontend.recv_multipart()
            backend.send_multipart(message)

        if socks.get(backend) == zmq.POLLIN:
            message = backend.recv_multipart()
            frontend.send_multipart(message)

if __name__ == '__main__':
    client = Process(target=client_main)
    #client2 = Process(target=client_main)
    #worker = Process(target=worker_main)
    worker_pool = [Process(target=worker_main) for x in range(4)]
    broker = Process(target=glue_main)
    broker.start()
    #worker.start()
    #worker2.start()
    for worker in worker_pool:
        worker.start()
    client.start()
    #client2.start()
