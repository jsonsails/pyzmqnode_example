__author__ = 'eujain'
""" Show example of multiple workers and clients behaving asynchronously.
 Use case is having several Node workers going at once.
 However, note that it is still synchronous: the client waits for a response
 before sending out a new request.
 I thought that the queue would help but it doesn't.
 """

from multiprocessing import Process
import zmq
import os

#
#   Request-reply client in Python
#   Connects REQ socket to tcp://localhost:5559
#   Sends "Hello" to server, expects "World" back
#

def client_main():
    #  Prepare our context and sockets
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5559")

    #  Do 10 requests, waiting each time for a response
    for request in range(1,31):
        socket.send(b"Hello")
        message = socket.recv()
        print("Received reply %s [%s]" % (request, message))

def worker_main():
    #
    #   Request-reply service in Python
    #   Connects REP socket to tcp://localhost:5560
    #   Expects "Hello" from client, replies with "World"
    #
    from time import sleep
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.connect("tcp://localhost:5560")

    while True:
        message = socket.recv()
        # fake processing
        sleep(0.1)
        pid = os.getpid()
        print("%s Received request: %s" % (pid, message))
        socket.send(b"World")

def glue_main():
    """

       Simple message queuing broker
       Same as request-reply broker but using QUEUE device

       Author: Guillaume Aubert (gaubert) <guillaume(dot)aubert(at)gmail(dot)com>

    """

    context = zmq.Context()

    # Socket facing clients
    frontend = context.socket(zmq.ROUTER)
    frontend.bind("tcp://*:5559")

    # Socket facing services
    backend = context.socket(zmq.DEALER)
    backend.bind("tcp://*:5560")

    zmq.device(zmq.QUEUE, frontend, backend)

    # We never get here…
    frontend.close()
    backend.close()
    context.term()


if __name__ == '__main__':
    client = Process(target=client_main)
    #client2 = Process(target=client_main)
    #worker = Process(target=worker_main)
    worker_pool = [Process(target=worker_main) for x in range(4)]
    broker = Process(target=glue_main)
    broker.start()
    #worker.start()
    #worker2.start()
    for worker in worker_pool:
        worker.start()
    client.start()
    #client2.start()
