Description
===========

Several different ways of using ZeroMQ from Python.

Files
=====

multiprocessing_example.py
  python to python interprocess communication with zmq.

helloworld.js/py 
  a server/client example of talking to Node.

dealer_router.py
  first attempt at utilizing the dealer/router sockets that are said to be
  async, however the example exhibits sync behavior.

router_dealer_queued.py
  the second attempt, using an example of a queue
  device in the broker between clients and workers. This also appears synchronous.

dealer_router_async.py
  the third attempt, and uses threads to represent different Django threads
  each handling requests. This model is async as a whole, while its client
  threads internally still are blocking on IO.

dealer_to_rep.py
  the 4th method, using async dealer socket on the client instead of a REQ
  blocking socket. In this example the client code may progress without waiting
  for a response from the server.

async_example.py
  code from the web demonstrating async client+server behavior.
