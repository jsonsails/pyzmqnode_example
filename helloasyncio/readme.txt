Description
===========

asyncio is a new core library added to Python 3.4

It offers asynchronous cooperative threading using "yield from" however it
appears to "infect" code and all code would have to be written in the new
paradigm.

aiozmq is a library that provides asyncio interface to pyzmq which is the
Python wrapper around ZeroMQ.

Files
=====

helloworld.py 
  is the official asyncio starter.

mp_test.py 
  is a first attempt at using coroutines with zmq directly

helloaiozmq.py
  an example of using the low level aiozmq library.

hellostreams.py
  an example using high level aiozmq streams.


