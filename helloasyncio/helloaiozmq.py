import zmq
from multiprocessing import Process
import asyncio
import aiozmq
import time
""" Use aiozmq library for asyncio interface to zmq """

class serverProcess(Process):
    def __init__(self):
        super().__init__()

    def run(self):
        @asyncio.coroutine
        def bar():
            server = yield from aiozmq.create_zmq_stream(
                zmq.REP,
                bind='tcp://127.0.0.1:5556'
            )
            while True:
                print("server ready.")
                msg = yield from server.read()
                print("server got " + msg[0].decode())
                if msg[0] == b'kill':
                    break
                server.write([msg[0] + b" world"])
                print("server sent response")
            server.close()

        asyncio.get_event_loop().run_until_complete(bar())

@asyncio.coroutine
def foo():
    """goal is to be able to use this code style asynchronously """
    client = yield from aiozmq.create_zmq_stream(
        zmq.REQ,
        connect='tcp://127.0.0.1:5556'
    )
    print("starting client")
    client.write([b"hello"])
    value = yield from client.read()
    print(value[0].decode())
    client.write([b'kill'])
    client.close()

def main():
    print("boo")
    eventloop = asyncio.get_event_loop()
    server = serverProcess()
    print("starting server")
    server.start()
    eventloop.run_until_complete(foo())

if __name__ == "__main__":
    main()