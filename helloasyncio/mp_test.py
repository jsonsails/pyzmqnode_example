import zmq
from multiprocessing import Process
import asyncio
import time
""" Wrapping zmq with coroutines for synchronous code style. """

class serverProcess(Process):
    """simulate a server"""
    def __init__(self):
        super().__init__()

    def run(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("tcp://127.0.0.1:5556")
        while True:
            msg = self.socket.recv_string()
            print("server got " + msg)
            self.socket.send_string(msg + " world")
            print("server sent response")

@asyncio.coroutine
def synchronousCode(server):
    """goal is to be able to use this code style asynchronously """
    value = yield from server.req("hello")
    print(value)
    server.terminate()

class ServerWrapper:
    """async wrapper around remote server"""
    def __init__(self, eloop):
        self.server = serverProcess()
        self.server.start()
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect("tcp://127.0.0.1:5556")
        self.poller = zmq.Poller()
        self.poller.register(self.socket, flags=zmq.POLLIN)
        self.eloop = eloop

    def req(self, val):
        self.socket.send_string(val)
        print("sent " + val)
        future_resp = asyncio.futures.Future()
        self.recv(future_resp)
        return future_resp

    def recv(self, future_resp):
        print("recv")
        if future_resp.cancelled():
            return
        # need to poll to avoid blocking
        events = self.poller.poll()
        if len(events) == 0:
            self.eloop.call_soon(self.recv, future_resp)
        else:
            resp = self.socket.recv_string()
            print("got response: " + resp)
            future_resp.set_result(resp)

    def terminate(self):
        self.server.terminate()


def main():
    eventloop = asyncio.get_event_loop()
    server = ServerWrapper(eventloop)
    time.sleep(1)
    eventloop.run_until_complete(synchronousCode(server))

if __name__ == "__main__":
    main()
