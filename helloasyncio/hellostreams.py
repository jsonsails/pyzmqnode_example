import asyncio
from multiprocessing import Process
import time

class serverProcess(Process):
    def __init__(self):
        super().__init__()

    def run(self):
        @asyncio.coroutine
        def bar(reader, writer):
            print("in server")
            data = yield from reader.read(100)
            msg = data.decode()
            msg += " world"
            writer.write(msg.encode())
            yield from writer.drain()
            print("close server")
            writer.close()

        loop = asyncio.get_event_loop()
        print("starting server")
        server = asyncio.start_server(bar, '127.0.0.1', 5555, loop=loop)
        loop.run_until_complete(server)
        print("server started")
        loop.run_forever()


@asyncio.coroutine
def foo(loop):
    reader, writer = yield from asyncio.open_connection(
        '127.0.0.1', 5555, loop=loop
    )
    writer.write("hello".encode())
    data = yield from reader.read()
    msg = data.decode()
    print(msg)
    writer.close()

def main():
    server = serverProcess()
    server.start()
    loop = asyncio.get_event_loop()
    time.sleep(1) # hack to be sure server is up
    # this is what zmq handles for us!
    print("start client")
    loop.run_until_complete(foo(loop))

if __name__ == "__main__":
    main()