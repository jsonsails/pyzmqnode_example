package manager has outdated version of libzmq: latest is v4.1.1
installing from source: configure fails on libsodium not found
libsodium is not in package manager for Ubuntu. Build from source.
    follow http://doc.libsodium.org/installation/index.html
    sudo make install
./configure again on libzmq 4.1.1
sudo make install
next need to update library cache:
    sudo ldconfig

Install zmq in the virtualenv: pip install pyzmq

Make sure NodeJS is installed. Nodejs_legacy allows calling it with 'node'
instead of 'nodejs'

Install zeromq.node: https://github.com/JustinTulloss/zeromq.node
    npm install zmq